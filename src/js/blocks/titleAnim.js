import gsap from "gsap";
import ScrollTrigger from "gsap/ScrollTrigger";

export function titleAnimation() {
	const block = document.querySelector(".exams__title-block");

	gsap.to(block, {
		y: "100%",
		ease: "expo.out",
		duration: 1.4,
		scrollTrigger: {
			trigger: block,
			start: "top 70%",
		}
	})
	gsap.fromTo(".exams__title", {
		opacity: 0,
		y: 50
	}, {
		y: 0,
		opacity: 1,
		ease: "expo.out",
		duration: 2,
		scrollTrigger: {
			trigger: block,
			start: "top 70%",
		}
	})

	const block2 = document.querySelector(".programms__title-block");

	gsap.to(block2, {
		y: "100%",
		ease: "expo.out",
		duration: 1.4,
		scrollTrigger: {
			trigger: block2,
			start: "top 70%",
		}
	})
	gsap.fromTo(".programms__title", {
		// opacity: 0,
		y: 50
	}, {
		y: 0,
		// opacity: 1,
		ease: "expo.out",
		duration: 2,
		scrollTrigger: {
			trigger: block2,
			start: "top 70%",
		}
	})

	const block3 = document.querySelector(".possibilities__title-block");

	gsap.to(block3, {
		y: "100%",
		ease: "expo.out",
		duration: 1.4,
		scrollTrigger: {
			trigger: block3,
			start: "top 70%",
		}
	})
	gsap.fromTo(".possibilities__title", {
		// opacity: 0,
		y: 50
	}, {
		y: 0,
		// opacity: 1,
		ease: "expo.out",
		duration: 2,
		scrollTrigger: {
			trigger: block3,
			start: "top 70%",
		}
	})

	const block4 = document.querySelector(".location__title-block");

	gsap.to(block4, {
		y: "100%",
		ease: "expo.out",
		duration: 1.4,
		scrollTrigger: {
			trigger: block4,
			start: "top 70%",
		}
	})
	gsap.fromTo(".location__title", {
		// opacity: 0,
		y: 50
	}, {
		y: 0,
		// opacity: 1,
		ease: "expo.out",
		duration: 2,
		scrollTrigger: {
			trigger: block4,
			start: "top 70%",
		}
	})

	const block5 = document.querySelector(".method__title-block");

	gsap.to(block5, {
		y: "100%",
		ease: "expo.out",
		duration: 1.4,
		scrollTrigger: {
			trigger: block5,
			start: "top 70%",
		}
	})
	gsap.fromTo(".method__title", {
		// opacity: 0,
		y: 50
	}, {
		y: 0,
		// opacity: 1,
		ease: "expo.out",
		duration: 2,
		scrollTrigger: {
			trigger: block5,
			start: "top 70%",
		}
	})

	gsap.fromTo(".exams__feautures-img img", {
		y: "100%"
	}, {
		y: 0,
		ease: "expo.out",
		duration: 2,
		scrollTrigger: {
			trigger: ".exams__feautures",
			start: "top 70%",
		}
	})

	gsap.fromTo(".possibilities__content-img", {
		y: "100%"
	}, {
		y: 0,
		ease: "expo.out",
		duration: 2,
		scrollTrigger: {
			trigger: ".possibilities__content",
			start: "top 50%",
		}
	})

	gsap.fromTo(".programms__content", {
		y: "100%"
	}, {
		y: 0,
		ease: "expo.out",
		duration: 2,
		scrollTrigger: {
			trigger: ".programms",
			start: "top 50%",
		}
	})
};
