import Swiper from 'swiper';
import SwiperCore, { Navigation, Pagination } from 'swiper/core';

SwiperCore.use([Navigation, Pagination]);

export function initExamSlider() {
	const swiper = new Swiper('.exams__container', {
		slidesPerView: 1,
		spaceBetween: 20,
		navigation: {
			nextEl: ".exams__nav-btn._right",
			prevEl: ".exams__nav-btn._left"
		},
		breakpoints: {
			501: {
				slidesPerView: 2,
				spaceBetween: 20,
			},
			768: {
				slidesPerView: 2,
				spaceBetween: 30,
			},
			960: {
				slidesPerView: 3,
				spaceBetween: 30,
			},
			1200: {
				slidesPerView: 4,
				spaceBetween: 40,
			},
		},
		pagination: {
			el: ".exams__slider-pagination",
			clickable: true,
		}
	});
};

