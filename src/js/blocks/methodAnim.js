import gsap from "gsap";
import ScrollTrigger from "gsap/ScrollTrigger";

export function animMethod() {
	const items = document.querySelectorAll(".method__item");

	items.forEach(function(element, index) {
		if (index == 0) {
			gsap.fromTo(element.querySelector(".method__item-img"), {
				y: "100%"
			}, {
				y: 0,
				duration: 1.4,
				ease: "expo.out",
				scrollTrigger: {
					trigger: element,
					start: "top 40%",
				}
			})
		} else if (index == 3) {
			let timeline = gsap.timeline({
				scrollTrigger: {
					trigger: element,
					start: "top 60%",
				}
			})
			timeline.fromTo(element.querySelector(".method__item-img"), {
				x: "100%"
			}, {
				x: 0,
				duration: 1.4,
				ease: "expo.out"
			}).fromTo(element.querySelector(".method__text-wrap h3"), {
				opacity: 0,
				x: "-100%"
			}, {
				opacity: 1,
				x: 0,
				ease: "power4.out",
			}, "-=0.6").fromTo(element.querySelector(".method__text-wrap p"), {
				opacity: 0,
				x: "-100%"
			}, {
				opacity: 1,
				x: 0,
				ease: "power4.out",
			}, "-=0.5")
		} else {
			let timeline2 = gsap.timeline({
				scrollTrigger: {
					trigger: element,
					start: "top 70%",
				}
			})
			timeline2.fromTo(element.querySelector(".method__item-img"), {
				y: "100%"
			}, {
				y: 0,
				duration: 1.4,
				ease: "expo.out",
			}).fromTo(element.querySelector(".method__text-wrap h3"), {
				opacity: 0,
				x: "-100%"
			}, {
				opacity: 1,
				x: 0,
				ease: "power4.out",
			}, "-=0.6").fromTo(element.querySelector(".method__text-wrap p"), {
				opacity: 0,
				x: "-100%"
			}, {
				opacity: 1,
				x: 0,
				ease: "power4.out",
			}, "-=0.5")
		}
	});
};
