import gsap from "gsap";
import ScrollTrigger from "gsap/ScrollTrigger";
import lazyImages from "../modules/lazyImages";

gsap.registerPlugin(ScrollTrigger);

export function initGallerySlider() {
	const tr = document.querySelector(".possibilities");
	const gallery = document.querySelectorAll(".our-class__gallery");
	const galleryTop = document.querySelector(".our-class__gallery._top ._wrapper");
	const galleryBottom = document.querySelector(".our-class__gallery._bottom ._wrapper");

	// galleryTop.querySelectorAll(".our-class__slide").forEach(function(el){
	// 	galleryTop.appendChild(el.cloneNode(true));
	// })

	// galleryBottom.querySelectorAll(".our-class__slide").forEach(function(el){
	// 	galleryBottom.appendChild(el.cloneNode(true));
	// })

	lazyImages();

	gallery.forEach(function(item, index){
		const wrapper = item.querySelector("._wrapper")
		const [x, xEnd] = (index % 2) ? ['100%', (wrapper.scrollWidth - item.offsetWidth) * -1] : [wrapper.scrollWidth * -1, 0];

		gsap.fromTo(wrapper, {
			x
		}, {
			x: xEnd,
			scrollTrigger: {
				trigger: tr,
				scrub: 1
			}
		})
	});
};
