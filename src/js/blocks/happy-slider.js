import Swiper from 'swiper';
import SwiperCore, { Navigation, Pagination } from 'swiper/core';

SwiperCore.use([Navigation, Pagination]);


export function initHappySlider() {
	const shappySwiper = new Swiper('.happy__container', {
		slidesPerView: 1,
		spaceBetween: 20,
		navigation: {
			nextEl: ".happy__nav-btn._right",
			prevEl: ".happy__nav-btn._left"
		},
		breakpoints: {
			501: {
				slidesPerView: 1,
				spaceBetween: 20,
			},
			768: {
				slidesPerView: 2,
				spaceBetween: 30,
			},
			1200: {
				slidesPerView: 3,
				spaceBetween: 40,
			},
		},
		pagination: {
			el: ".happy__slider-pagination",
			clickable: true,
		}
	});
};
