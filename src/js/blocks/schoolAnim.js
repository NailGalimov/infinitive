import gsap from "gsap";
import ScrollTrigger from "gsap/ScrollTrigger";

export function anim() {
	const items = document.querySelectorAll(".school__item");
	const timeline = gsap.timeline({
		scrollTrigger: {
			trigger: ".school",
			start: "top 50%",
		}
	})

	function startCartAnim () {
		items.forEach(function(element) {
			const tl = gsap.timeline({
				scrollTrigger: {
					trigger: element,
					start: "top 80%",
					end: "bottom bottom",
					scrub: 0.5,
				}
			});

			tl.fromTo(element, {
				opacity: 0,
				y: 176
			}, {
				y: 0,
				opacity: 1,
				duration: 0.9,
				ease: "power4.out",
			})
		});
	}

	timeline.fromTo("#line", {
		strokeDashoffset: 5700,
	}, {
		strokeDashoffset: 0,
		duration: 3,
		ease: "expo.out",
		onComplete: startCartAnim()
	})

	gsap.utils.toArray(".parallax").forEach(layer => {
		const depth = layer.dataset.depth;
		const movement = -(layer.offsetHeight * depth)
		gsap.to(layer, {
			y: movement,
			ease: "none",
			scrollTrigger: {
				trigger: layer,
				start: "top 50%",
				end: "bottom top",
				scrub: true
			}
		}, 0)
	});
};
