import gsap from "gsap";

export default () => {
	const tabs = document.querySelectorAll(".tabs-1");
	const tabsButton = document.querySelectorAll(".tabs__button-1");
	const tabsContent = document.querySelectorAll(".tabs__content-1");

	const timeline = gsap.timeline({
		paused: true,
		reversed: true
	})

	if (tabs) {
		tabs.forEach(function(el){
			el.addEventListener("click", function(e){
				if(e.target.classList.contains("tabs__button-1")) {
					const tabsPath = e.target.dataset.tabsParent1;

					el.querySelectorAll(".tabs__button-1").forEach(function(el){ el.classList.remove("_active") })
					el.querySelector(`[data-tabs-parent1="${tabsPath}"]`).classList.add("_active");

					let currentNavItem = el.querySelector(`[data-tabs-parent1="${tabsPath}"]`);
					let currentContentItem = el.querySelector(`[data-tabs-child-1="${tabsPath}"]`);
					let tabsImg = currentContentItem.querySelector("._wrap");

					timeline
						.fromTo(tabsImg, {
							y: "100%",
						}, {
							y: 0,
							ease: "expo.out",
							duration: 1,
						})

					el.querySelectorAll(".tabs__button-1").forEach(function(el) {el.classList.remove("_active")});
					currentNavItem.classList.add("_active");

					el.querySelectorAll(".tabs__content-1").forEach(function(el) {
						el.classList.remove("_active");

						timeline.reverse();
					});
					currentContentItem.classList.add("_active");

					timeline.play();

					if (window.matchMedia("(max-width: 600px)").matches) {
						document.querySelector(".programms").scrollIntoView({
							behavior: 'smooth',
							block: 'end'
						})
					}
				}
			});
		})
	}
}
