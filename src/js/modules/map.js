export default () => {
	const yaMap = document.querySelector(".ya_map");
	const adressItem = document.querySelectorAll(".location__adress-item");

	if (typeof ymaps === 'undefined') {
		return;
	}

	ymaps.ready(function () {
		let coords = [59.942584, 30.358026];

		let myMap = new ymaps.Map('ymap', {
			center: coords,
			zoom: 16
		});

		myMap.behaviors.disable('scrollZoom');

		// Добавление меток
		function addPlacemark(coords, baloonText) {
			var myPlacemark = new ymaps.Placemark(coords, {
				balloonContent: baloonText
			}, {
				iconLayout: 'default#image',
				iconImageHref: '/img/marker.svg',
				iconImageSize: [51, 59],
				iconImageOffset: [-51, -59]
			});

			myMap.geoObjects.add(myPlacemark)
		}

		addPlacemark(coords, "ул. Рылеева, д. 10");

		adressItem.forEach(function(el){
			el.addEventListener("click", function(){
				let attr = this.getAttribute("data-adress");
				let myGeocoder = ymaps.geocode(attr);

				adressItem.forEach(function(el){el.classList.remove("_active")});
				this.classList.add("_active");

				document.getElementById("ymap").scrollIntoView({
					behavior: 'smooth',
					block: 'center'
				})

				myGeocoder.then(
					function (res) {
						var center = res.geoObjects.get(0).geometry.getCoordinates();

						myMap.setCenter(center, 16, {
							duration: 100,
							timingFunction: "ease-out"
						});
						myMap.geoObjects.removeAll();
						addPlacemark(center, attr);
					},
					function (err) {

					}
				);
			});

			el.addEventListener("focus", function(){
				let attr = this.getAttribute("data-adress");
				let myGeocoder = ymaps.geocode(attr);

				adressItem.forEach(function(el){el.classList.remove("_active")});
				this.classList.add("_active");

				document.getElementById("ymap").scrollIntoView({
					behavior: 'smooth',
					block: 'center'
				})

				myGeocoder.then(
					function (res) {
						var center = res.geoObjects.get(0).geometry.getCoordinates();

						myMap.setCenter(center, 16, {
							duration: 100,
							timingFunction: "ease-out"
						});
						myMap.geoObjects.removeAll();
						addPlacemark(center, attr);
					},
					function (err) {

					}
				);
			});

		})

		document.querySelector(".show-more._last").addEventListener("click", function(){
			this.closest(".location__adress-list").classList.add("show-full");
			this.style.display = "none";
		})
	});
}
