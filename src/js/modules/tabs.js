import gsap from "gsap";

export default () => {
	const tabs = document.querySelectorAll(".tabs");
	const tabsButton = document.querySelectorAll(".tabs__button");
	const tabsContent = document.querySelectorAll(".tabs__content");

	const timeline = gsap.timeline({
		paused: true,
		reversed: true
	})

	if (tabs) {
		tabs.forEach(function(el){
			el.addEventListener("click", function(e){
				if(e.target.classList.contains("tabs__button")) {
					const tabsPath = e.target.dataset.tabsParent;

					el.querySelectorAll(".tabs__button").forEach(function(el){ el.classList.remove("_active") })
					el.querySelector(`[data-tabs-parent="${tabsPath}"]`).classList.add("_active");

					let currentNavItem = el.querySelector(`[data-tabs-parent="${tabsPath}"]`);
					let currentContentItem = el.querySelector(`[data-tabs-child="${tabsPath}"]`);
					let tabsImg = currentContentItem.querySelector(".possibilities__content-img");

					timeline
						.fromTo(tabsImg, {
							x: "100%",
						}, {
							x: 0,
							ease: "expo.out",
							duration: 1,
						}).fromTo(currentContentItem.querySelector(".possibilities__content-info"), {
							x: "-100%",
							opacity: 0,
						}, {
							x: 0,
							opacity: 1,
							ease: "expo.out",
							duration: 1,
						}, "-=0.9")

					el.querySelectorAll(".tabs__button").forEach(function(el) {el.classList.remove("_active")});
					currentNavItem.classList.add("_active");

					el.querySelectorAll(".tabs__content").forEach(function(el) {
						el.classList.remove("_active");

						timeline.reverse();
					});
					currentContentItem.classList.add("_active");

					timeline.play();

					if (window.matchMedia("(max-width: 600px)").matches) {
						currentContentItem.closest(".possibilities__content").scrollIntoView({
							behavior: 'smooth',
							block: 'end'
						})
					}
				}
			});
		})
	}
}
