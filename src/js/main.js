import documentReady from "./helpers/documentReady";
import lazyImages from "./modules/lazyImages";
import { initExamSlider } from "./blocks/exams-slider";
import { initGallerySlider } from "./blocks/our-class-gallery";
import { initHappySlider } from "./blocks/happy-slider";
import map from "./modules/map";
import tabs from "./modules/tabs";
import { anim } from "./blocks/schoolAnim";
import { animMethod } from "./blocks/methodAnim";
import { titleAnimation } from "./blocks/titleAnim";
import programmsTab from "./blocks/programmsTab";
import gsap from "gsap";

window.onload = function () {
    document.body.classList.add('loading');
	let timeline = gsap.timeline();

	timeline.set(".preloader img", {
		position: "absolute",
		x: "-50%",
		y: "-50%",
		top: "50%",
		left: "50%",
		opacity: 1,
		scale: 1.5,
	}).to(".preloader img", {
		x: 0,
		y: 0,
		top: "auto",
		left: "auto",
		ease: "expo.out",
		duration: 1.5,
		scale: 1,
		onComplete: function hidePreloader() {
			document.querySelector(".preloader").style.display = "none";
			document.body.classList.remove('loading');

			heroWorsAnimation();
		}
	}, "+=0.9")


	function heroWorsAnimation() {
		const heroWords = document.querySelectorAll(".free-speak__texts img");
		const wordstimeLine = gsap.timeline();

		wordstimeLine.fromTo(".free-speak__img img", {
			y: "100%",
		}, {
			y: 0,
			ease: "expo.out",
			duration: 0.9
		})

		if (window.matchMedia("(min-width: 1200px)").matches) {
			heroWords.forEach(function(item){
				wordstimeLine.to(item, {
					x: "30%",
					opacity: 1,
					scale: 1,
					ease: "expo.out",
					duration: 3
				})
				.to(item, {}, 2, {})
				.to(item, {
					x: "80%",
					opacity: 0,
					scale: 1.4,
					ease: "expo.out",
					duration: 3
				})
			});
		}

	}
}

documentReady(() => {
	lazyImages();
	initExamSlider();
	initGallerySlider();
	initHappySlider();
	tabs();
	map();
	anim();
	animMethod();
	titleAnimation();
	programmsTab();


	document.querySelectorAll(".exams__slide-button").forEach(function(el){
		el.addEventListener("focus", function(){
			this.closest(".exams__slide").classList.add("_fucused");
		});
		el.addEventListener("blur", function(){
			this.closest(".exams__slide").classList.remove("_fucused");
		});
	})

	const happySlide = document.querySelectorAll(".happy__slide");

	happySlide.forEach(function(item){
		const textWrap = item.querySelector("._wrap");
		const happyReadMoreButton = item.querySelector(".happy__read-more");

		happyReadMoreButton.querySelector(".hide-text").style.display = "none";
		// textWrap.classList.add("_hidden-text");

		if (textWrap.offsetHeight > 168) {
			textWrap.style.height = "168px";
			textWrap.style.overflow = "hidden";
			item.querySelector(".happy__read-more").style.display = "inline-block";
		} else {
			item.querySelector(".happy__read-more").style.display = "none";
		}

		happyReadMoreButton.addEventListener("click", function(e){
			if (!e.target.classList.contains("_active")) {
				this.closest(".happy__slide").querySelector("._wrap").style.height = "auto";
				this.querySelector(".read-more").style.display = "none";
				this.querySelector(".hide-text").style.display = "inline-block";
				this.classList.add("_active");
			} else {
				this.closest(".happy__slide").querySelector("._wrap").style.height = "168px";
				this.querySelector(".read-more").style.display = "inline-block";
				this.querySelector(".hide-text").style.display = "none";
				this.classList.remove("_active");
			}
		});
	})

});
